import { Component } from '@angular/core';
import {RealTimeService} from './Services/real-time.service';

@Component({
    selector: 'app-stone-twitter',
    template: `
        <input #stoneTwitter>
        <div>Search by: "{{searchBy}}"</div>
        <div>UpDate Time: "{{upDateTime | date :  "dd.MM.y H:mm"}}"</div>
        <button (click)="runNewSocket(stoneTwitter.value)">Add</button>

        <ul><li *ngFor="let twitter of twittersList">Date:{{twitter.created | date :  "dd.MM.y H:mm"}} Text: {{twitter.text}}</li></ul>
    `
})

export class StoneTwitterComponent {
    twittersList = [];
    runSocket = true;
    searchBy = '';
    upDateTime;
    constructor(private myWS: RealTimeService) {
    }


    runNewSocket(text) {
        this.myWS.SendME(text);
        this.searchBy = text;
        if (this.runSocket) {
            this.runSocket = false;
            this.myWS.getFromEvent().subscribe(
                result => {
                    this.upDateTime = new Date();
                    this.twittersList = result;
                },
                error => {
                    console.log(error);
                });

        }
}
}
