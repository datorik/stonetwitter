import { RealTimeService } from './Services/real-time.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
const config: SocketIoConfig = { url: 'http://localhost:3000', options: {} };

import { AppComponent } from './app.component';
import { StoneTwitterComponent } from './app-stone-twitter.component';

@NgModule({
  declarations: [
    AppComponent,
    StoneTwitterComponent
  ],
  imports: [
    BrowserModule,
    SocketIoModule.forRoot(config),
  ],
  providers: [RealTimeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
