import {Injectable} from '@angular/core';
import {Socket, SocketIoModule} from 'ng-socket-io';
import 'rxjs/add/operator/map';
import * as io from 'socket.io-client';
@Injectable()
export class RealTimeService {
    constructor(private mysocket: Socket) {
    }

    SendME(message) {
        this.mysocket.emit('SEND', message);
    }

    getFromEvent() {
        return this.mysocket
            .fromEvent<any>('HiThere')
            .map(data => data);
    }
}
